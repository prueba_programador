#!/usr/bin/env python

if __name__ == "__main__":

    import random

    from pymongo import MongoClient

    client = MongoClient()
    db = client.mydb

    # Insert
    document = {str(i): random.randint(1, 10000) for i in range(1000)}
    inserted = db.mycollection.insert_one(document)

    # Query
    document = db.mycollection.find_one({"_id": inserted.inserted_id})

    # Print
    del document['_id']
    document_values = sorted(document.values())
    print('El valor mínimo es: ' + str(document_values[0]))
    print('El valor máximo es: ' + str(document_values[-1]))
    diez_valores_al_azar = [random.choice(document_values) for i in range(10)]
    print('10 valores al azar son: ' + str(diez_valores_al_azar))
